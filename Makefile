InstallToolkit.app: install/installer.scpt src/MIDIClass.js src/factory.js
	rm -rf InstallToolkit.app
	/usr/bin/osacompile -o InstallToolkit.app install/installer.scpt
	cp -f src/factory.js InstallToolkit.app/Contents/Resources/.
	cp -f src/MIDIClass.js InstallToolkit.app/Contents/Resources/.
