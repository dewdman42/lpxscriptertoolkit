This project will provide a tool for hyper loading extra functions and data into the LogicPro `MIDIClass.js` file.  This will only work as long as LPX continues to use that file for providing extra midi functions in LogicPro Scripter.

See the wiki: https://gitlab.com/dewdman42/lpxscriptertoolkit/-/wikis/home

Submit Feature requests here: https://gitlab.com/dewdman42/lpxscriptertoolkit/-/issues  _(you have to make a free account on gitlab to submit requests here)_

or just email me.  ;-)  steve@bstage.com

# Installation

## Easy Option

Download the latest Installer from the release section:  TBD

## Slightly Harder Option

You can build the installer yourself very easily.  This will ahve the result of having the very latest additions.

1. Clone this repo from the command line:

```bash
git clone https://gitlab.com/dewdman42/lpxscriptertoolkit.git
```

2. cd into the directory that was created and run `make`:

```bash
cd lpxscriptertoolkit
make
```

That's it.  An app called `InstallToolkit` will be created.  Run it and select either `Install` or `Recover Factory`


