-- InstallLPXToolkit 
-- 
-- Install Customized verison of MIDIClass.js into LogicPro application bundle
--
-- Source files are contained in this saved bundle.
--
--------------------------------------------------------------------------------

-- set path names in format suitable for sh 

-- path of source extended MIDIClass.js
set mf to path to resource "MIDIClass.js"
set mfname to POSIX path of mf
-- path of factory version in case they want to restore (currently LPX 10.5.1)
set uf to path to resource "factory.js"
set ufname to POSIX path of uf

-- set destination folder
set dest to "\"/Applications/Logic Pro X.app/Contents/Frameworks/MADSP.framework/Resources/\""

-- create cp commands
set command to "cp -f " & mfname & " " & dest
set ucommand to "cp -f " & ufname & " " & dest & "MIDIClass.js"

-- display simply dialog asking whether to install or restore factory
display dialog "LogicPro Scripter Toolkit Extension" buttons {"Install", "Restore Factory", "Cancel"} default button "Install"

-- respond to dialog
if result = {button returned:"Install"} then
	do shell script command
else if result = {button returned:"Restore Factory"} then
	do shell script ucommand
end if


