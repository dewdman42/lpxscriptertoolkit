/*************************************************************
 * MIDIClass.js - Extended by LogicPro Scripter Toolkit
 *
 * version:  0.04
 *
 * More info here: https://gitlab.com/dewdman42/lpxscriptertoolkit/-/wikis/home
 *
 *************************************************************/

/************************************************************************
 * MIDIClass.js sourced from LogicPro 10.5.1 - 10.7.9
 *
 * Path:
 *
 * /Applications/Logic\ Pro\ X.app/Contents/Frameworks/MADSP.framework/Resources/MIDIClass.js
 *
 *
 ************************************************************************/

//-----------------------------------------------------------------------------
// MIDIClass.js
// MIDI object (misc. functionality)
//-----------------------------------------------------------------------------

var MIDI = {};

//-----------------------------------------------------------------------------
MIDI.noteNumber = function(name) { 
	return this._noteNames.indexOf(name.toUpperCase()); 
};

//-----------------------------------------------------------------------------
MIDI.noteName = function(number) { 
	return this._noteNames[number];
};

//-----------------------------------------------------------------------------
MIDI.ccName = function(number) { 
	return this._ccNames[number];
};

//-----------------------------------------------------------------------------
MIDI.allNotesOff = function() {
	var cc = new ControlChange;
	cc.number = 123;
	cc.value = 0;
	this._sendEventOnAllChannels(cc);
}

//-----------------------------------------------------------------------------
MIDI._sendEventOnAllChannels = function(e) {
	for (var i=1; i <= 16; i++) {
		e.channel = i;
		e.send();
	}
}

//-----------------------------------------------------------------------------
MIDI.normalizeStatus = function(value) {
	if (value.constructor == Number) {
		if (value > 239)
			value = 239;
		else if (value < 128)
			value = 128;
		return parseInt(value);
	}
	else
		return 128;
};

//-----------------------------------------------------------------------------
MIDI.normalizeChannel = function(value) {
	if (value.constructor == Number) {
		if (value > 16)
			value = 16;
		else if (value < 1)
			value = 1;
		return parseInt(value);
	}
	else
		return 1;
};

//-----------------------------------------------------------------------------
MIDI.normalizeData = function(value)
{
	if (value.constructor == Number) {
		if (value > 127)
			value = 127;
		else if (value < 0)
			value = 0;
		return parseInt(value);
	}
	else
		return 0;
};

//-----------------------------------------------------------------------------
// Note/CC Enums
//-----------------------------------------------------------------------------
MIDI._makeNoteNames = function() {
  var noteNamesArray = [];
	var rootNames = ['C','C#','D','D#','E','F','F#','G','G#','A','A#','B'];
	for (var i=0; i < 128; i++) {
		var octaveName = Math.floor(i / 12) - 2;
		noteNamesArray.push(rootNames[i % 12] + octaveName);
	}
  return noteNamesArray;
}

MIDI._noteNames = Object.freeze(MIDI._makeNoteNames());
//-----------------------------------------------------------------------------
MIDI._ccNames = Object.freeze([
   'Bank MSB',
   'Modulation',
   'Breath',
   'Ctrl 3',
   'Foot Control',
   'Portamento',
   'Data MSB',
   'Volume',
   'Balance',
   'Ctrl 9',
   'Pan',
   'Expression',
   'Effect #1 MSB',
   'Effect #2 MSB',
   'Ctrl 14',
   'Ctrl 15',
   'General #1',
   'General #2',
   'General #3',
   'General #4',
   'Ctrl 20',
   'Ctrl 21',
   'Ctrl 22',
   'Ctrl 23',
   'Ctrl 24',
   'Ctrl 25',
   'Ctrl 26',
   'Ctrl 27',
   'Ctrl 28',
   'Ctrl 29',
   'Ctrl 30',
   'Ctrl 31',
   'Bank LSB',
   '#01 LSB',
   '#02 LSB',
   '#03 LSB',
   '#04 LSB',
   '#05 LSB',
   '#06 LSB',
   '#07 LSB',
   '#08 LSB',
   '#09 LSB',
   '#10 LSB',
   '#11 LSB',
   'Effect #1 LSB',
   'Effect #2 LSB',
   '#14 LSB',
   '#15 LSB',
   '#16 LSB',
   '#17 LSB',
   '#18 LSB',
   '#19 LSB',
   '#20 LSB',
   '#21 LSB',
   '#22 LSB',
   '#23 LSB',
   '#24 LSB',
   '#25 LSB',
   '#26 LSB',
   '#27 LSB',
   '#28 LSB',
   '#29 LSB',
   '#30 LSB',
   '#31 LSB',
   'Sustain',
   'Portamento',
   'Sostenuto',
   'Soft Pedal',
   'Legato',
   'Hold2',
   'Sound Var',
   'Resonance',
   'Release Time',
   'Attack Time',
   'Brightness',
   'Decay Time',
   'Vibrato Rate',
   'Vibrato Depth',
   'Vibrato Delay',
   'Ctrl 79',
   'Decay',
   'HPF Frequ',
   'General #7',
   'General #8',
   'Portamento Ctl',
   'Ctrl 85',
   'Ctrl 86',
   'Ctrl 87',
   'Ctrl 88',
   'Ctrl 89',
   'Ctrl 90',
   'Reverb',
   'Tremolo',
   'Chorus Depth',
   'Detune/Var.',
   'Phaser',
   'Data increm.',
   'Data decrem.',
   'Non-Reg. LSB',
   'Non-Reg. MSB',
   'Reg.Par. LSB',
   'Reg.Par. MSB',
   'Ctrl 102',
   'Ctrl 103',
   'Ctrl 104',
   'Ctrl 105',
   'Ctrl 106',
   'Ctrl 107',
   'Ctrl 108',
   'Ctrl 109',
   'Ctrl 110',
   'Ctrl 111',
   'Ctrl 112',
   'Ctrl 113',
   'Ctrl 114',
   'Ctrl 115',
   'Ctrl 116',
   'Ctrl 117',
   'Ctrl 118',
   'Ctrl 119',
   'All Sound Off',
   'Reset Ctrls.',
   'Local Control',
   'All Notes Off',
   'Omni Mode Off',
   'Omni Mode  On',
   'Mono Mode On',
   'Poly Mode On'
]);

/**************************************************************************
 ******  Custom Mods
 **************************************************************************/

/*
   Useful calculations

    let block = ctx.blockEndBeat - ctx.blockStartBeat;
    let beat = Math.floor(block);
    let fract = block * 16 % 16 / 16;
    let ticks = Math.round(960 * fract);
    let emins = block/ctx.tempo;
    let esecs = emins * 60;
    let ems = Math.round(esecs * 1000);
    let samples = Math.round(esecs * 48000);
*/   

var LPK = {};

/************************************************
 * MIDI function additions
 ************************************************/

/*************************************************
 * Beat and Time conversion functions
 *************************************************/

LPK.ticksFromBeat= function(beatPos, PPQN) {

    if(PPQN == undefined) PPQN = 960;

    let beat = Math.floor(beatPos);
    let fract = beatPos * 16 % 16 / 16;
    let ticks = Math.round(960 * fract);

    return ticks;
};

LPK.beatPosFromTicks = function(beat, ticks, PPQN) {

    if(PPQN == undefined) PPQN=960;

    beat = Math.round(beat); // round to nearest beat just in case
    let fract = ticks/PPQN;
    return beat + fract;
};


LPK.roundBeatPos = function(beat, PPQN) {

    let ticks = MIDI.ticksFromBeat(beat, PPQN);
    beat = Math.floor(beat);
    let fract = ticks / 960;      // assuming its what we want

    return beat + fract;
};

LPK.millisecondsFromBeats = function(beat, tempo) {
    return Math.round( 60000 * (beat/tempo));
};


LPK.samplesFromBeats = function(beat, tempo, rate) {
    let secs = 60 * (beat/tempo);
    return rate * secs;
};


LPK.beatsFromMilliseconds = function(ms, tempo) {
    let mins = ms/60000;
    return (mins * tempo);
};

LPK.beatsFromSamples = function(samples, tempo, rate) {
    let mins = samples/rate/60;
    return (mins * tempo);
};



/**********************************************
 * Range checking convenience functions
 **********************************************/

LPK.isPitchRange = function(event, low, high) {

    if(typeof low == "string") {
        low=MIDI.noteNumber(low);
    }
    if(typeof high == "string") {
        high=MIDI.noteNumber(high);
    }
    if(event.pitch >= low && event.pitch <= high) {
        return true;
    }
    else {
        return false;
    }
};

LPK.isValueRange = function(event, low, high) {
    if(event.value >= low && event.value <= high) {
        return true;
    }
    else {
        return false;
    }
};

LPK.isNumberRange = function(event, low, high) {
    if(event.value >= low && event.value <= high) {
        return true;
    }
    else {
        return false;
    }
};

LPK.isVelocityRange = function(event, low, high) {
    if(event.velocity >= low && event.velocity <= high) {
        return true;
    }
    else {
        return false;
    }
};


//=========================================
// LPK singletons
//=========================================

LPK._parameterCache = {
   data: [],
   set: function(id, val) {
            // TODO add error message if no PluginParameters
       if(typeof id != "string") id = PluginParameters[id].name;
       this.data[id] = val;
   },
   get: function(id) {
            // TODO add error message if no PluginParameters
       if(typeof id != "string") id = PluginParameters[id].name;
       if(this.data[id] == undefined) {
           this.data[id] = GetParameter(id);
       }
       return this.data[id];
   }
};
//=================================================
// _console singleton
// kept inside LPK, typically not accessed directly
//=================================================

LPK._console = {
   maxFlush: 20,
   maxLen:   1000,
   b:[],
   log: function(msg) {this.b.push(msg)},
   setMaxFlush: function(max) {this.maxFlush = max},
   setMaxLen: function(max) {this.maxLen = max},
   flush: function() {
       var i=0;
       while(i<=this.maxFlush && this.b.length>0) {
           var line=this.b.shift();
           if(typeof line != "string") {
               line = line.toString();
           }
           var tl = line.length;
           var p = 0;
           var sub=line.substr(p,this.maxLen);
           Trace(sub);
           p += this.maxLen;
           while(p < tl) {
               sub = line.substr(p,this.maxLen);
               Trace("..." + sub);
               p += this.maxLen;
           }
           i++;
       }
   }
};

//========================================
// Non callback wrapper functions
//========================================

LPK.Trace = function(msg) {
    LPK._console.log(msg);
};
LPK.GetParameter = function(id) {
    return LPK._parameterCache.get(id);
};
// simple wrapper for consistency.  will be cached by ParameterChanged
LPK.SetParameter = function(id, val) {
    SetParameter(id, val);
};
// required for parameter use
LPK.ParameterChanged = function(id, val) {
    LPK._parameterCache.set(id. val);
};
// required for buffered logging
LPK.Idle = function() {
    LPK._console.flush();
};

