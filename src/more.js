/*************************************************************
 * MIDIClass.js - Extended by LogicPro Scripter Toolkit
 *
 * version:  0.03
 *
 * More info here: https://gitlab.com/dewdman42/lpxscriptertoolkit/-/wikis/home
 *
 *************************************************************/

/************************************************************************
 * MIDIClass.js sourced from LogicPro 10.5.1 - 10.7.9
 *
 * Path:
 *
 * /Applications/Logic\ Pro\ X.app/Contents/Frameworks/MADSP.framework/Resources/MIDIClass.js
 *
 *
 ************************************************************************/

//-----------------------------------------------------------------------------
// MIDIClass.js
// MIDI object (misc. functionality)
//-----------------------------------------------------------------------------

var MIDI = {};

//-----------------------------------------------------------------------------
MIDI.noteNumber = function(name) { 
	return this._noteNames.indexOf(name.toUpperCase()); 
};

//-----------------------------------------------------------------------------
MIDI.noteName = function(number) { 
	return this._noteNames[number];
};

//-----------------------------------------------------------------------------
MIDI.ccName = function(number) { 
	return this._ccNames[number];
};

//-----------------------------------------------------------------------------
MIDI.allNotesOff = function() {
	var cc = new ControlChange;
	cc.number = 123;
	cc.value = 0;
	this._sendEventOnAllChannels(cc);
}

//-----------------------------------------------------------------------------
MIDI._sendEventOnAllChannels = function(e) {
	for (var i=1; i <= 16; i++) {
		e.channel = i;
		e.send();
	}
}

//-----------------------------------------------------------------------------
MIDI.normalizeStatus = function(value) {
	if (value.constructor == Number) {
		if (value > 239)
			value = 239;
		else if (value < 128)
			value = 128;
		return parseInt(value);
	}
	else
		return 128;
};

//-----------------------------------------------------------------------------
MIDI.normalizeChannel = function(value) {
	if (value.constructor == Number) {
		if (value > 16)
			value = 16;
		else if (value < 1)
			value = 1;
		return parseInt(value);
	}
	else
		return 1;
};

//-----------------------------------------------------------------------------
MIDI.normalizeData = function(value)
{
	if (value.constructor == Number) {
		if (value > 127)
			value = 127;
		else if (value < 0)
			value = 0;
		return parseInt(value);
	}
	else
		return 0;
};

//-----------------------------------------------------------------------------
// Note/CC Enums
//-----------------------------------------------------------------------------
MIDI._makeNoteNames = function() {
  var noteNamesArray = [];
	var rootNames = ['C','C#','D','D#','E','F','F#','G','G#','A','A#','B'];
	for (var i=0; i < 128; i++) {
		var octaveName = Math.floor(i / 12) - 2;
		noteNamesArray.push(rootNames[i % 12] + octaveName);
	}
  return noteNamesArray;
}

MIDI._noteNames = Object.freeze(MIDI._makeNoteNames());
//-----------------------------------------------------------------------------
MIDI._ccNames = Object.freeze([
   'Bank MSB',
   'Modulation',
   'Breath',
   'Ctrl 3',
   'Foot Control',
   'Portamento',
   'Data MSB',
   'Volume',
   'Balance',
   'Ctrl 9',
   'Pan',
   'Expression',
   'Effect #1 MSB',
   'Effect #2 MSB',
   'Ctrl 14',
   'Ctrl 15',
   'General #1',
   'General #2',
   'General #3',
   'General #4',
   'Ctrl 20',
   'Ctrl 21',
   'Ctrl 22',
   'Ctrl 23',
   'Ctrl 24',
   'Ctrl 25',
   'Ctrl 26',
   'Ctrl 27',
   'Ctrl 28',
   'Ctrl 29',
   'Ctrl 30',
   'Ctrl 31',
   'Bank LSB',
   '#01 LSB',
   '#02 LSB',
   '#03 LSB',
   '#04 LSB',
   '#05 LSB',
   '#06 LSB',
   '#07 LSB',
   '#08 LSB',
   '#09 LSB',
   '#10 LSB',
   '#11 LSB',
   'Effect #1 LSB',
   'Effect #2 LSB',
   '#14 LSB',
   '#15 LSB',
   '#16 LSB',
   '#17 LSB',
   '#18 LSB',
   '#19 LSB',
   '#20 LSB',
   '#21 LSB',
   '#22 LSB',
   '#23 LSB',
   '#24 LSB',
   '#25 LSB',
   '#26 LSB',
   '#27 LSB',
   '#28 LSB',
   '#29 LSB',
   '#30 LSB',
   '#31 LSB',
   'Sustain',
   'Portamento',
   'Sostenuto',
   'Soft Pedal',
   'Legato',
   'Hold2',
   'Sound Var',
   'Resonance',
   'Release Time',
   'Attack Time',
   'Brightness',
   'Decay Time',
   'Vibrato Rate',
   'Vibrato Depth',
   'Vibrato Delay',
   'Ctrl 79',
   'Decay',
   'HPF Frequ',
   'General #7',
   'General #8',
   'Portamento Ctl',
   'Ctrl 85',
   'Ctrl 86',
   'Ctrl 87',
   'Ctrl 88',
   'Ctrl 89',
   'Ctrl 90',
   'Reverb',
   'Tremolo',
   'Chorus Depth',
   'Detune/Var.',
   'Phaser',
   'Data increm.',
   'Data decrem.',
   'Non-Reg. LSB',
   'Non-Reg. MSB',
   'Reg.Par. LSB',
   'Reg.Par. MSB',
   'Ctrl 102',
   'Ctrl 103',
   'Ctrl 104',
   'Ctrl 105',
   'Ctrl 106',
   'Ctrl 107',
   'Ctrl 108',
   'Ctrl 109',
   'Ctrl 110',
   'Ctrl 111',
   'Ctrl 112',
   'Ctrl 113',
   'Ctrl 114',
   'Ctrl 115',
   'Ctrl 116',
   'Ctrl 117',
   'Ctrl 118',
   'Ctrl 119',
   'All Sound Off',
   'Reset Ctrls.',
   'Local Control',
   'All Notes Off',
   'Omni Mode Off',
   'Omni Mode  On',
   'Mono Mode On',
   'Poly Mode On'
]);

/**************************************************************************
 ******  Custom Mods
 **************************************************************************/

/*
   Useful calculations

    let block = ctx.blockEndBeat - ctx.blockStartBeat;
    let beat = Math.floor(block);
    let fract = block * 16 % 16 / 16;
    let ticks = Math.round(960 * fract);
    let emins = block/ctx.tempo;
    let esecs = emins * 60;
    let ems = Math.round(esecs * 1000);
    let samples = Math.round(esecs * 48000);
*/   

var LPK = {};

/************************************************
 * MIDI function additions
 ************************************************/

/*************************************************
 * Beat and Time conversion functions
 *************************************************/

LPK.ticksFromBeat= function(beatPos, PPQN) {

    if(PPQN == undefined) PPQN = 960;

    let beat = Math.floor(beatPos);
    let fract = beatPos * 16 % 16 / 16;
    let ticks = Math.round(960 * fract);

    return ticks;
};

LPK.beatPosFromTicks = function(beat, ticks, PPQN) {

    if(PPQN == undefined) PPQN=960;

    beat = Math.round(beat); // round to nearest beat just in case
    let fract = ticks/PPQN;
    return beat + fract;
};


LPK.roundBeatPos = function(beat, PPQN) {

    let ticks = MIDI.ticksFromBeat(beat, PPQN);
    beat = Math.floor(beat);
    let fract = ticks / 960;      // assuming its what we want

    return beat + fract;
};

LPK.millisecondsFromBeats = function(beat, tempo) {
    return Math.round( 60000 * (beat/tempo));
};


LPK.samplesFromBeats = function(beat, tempo, rate) {
    let secs = 60 * (beat/tempo);
    return rate * secs;
};


LPK.beatsFromMilliseconds = function(ms, tempo) {
    let mins = ms/60000;
    return (mins * tempo);
};

LPK.beatsFromSamples = function(samples, tempo, rate) {
    let mins = samples/rate/60;
    return (mins * tempo);
};



/**********************************************
 * Range checking convenience functions
 **********************************************/

LPK.isPitchRange = function(event, low, high) {

    if(typeof low == "string") {
        low=MIDI.noteNumber(low);
    }
    if(typeof high == "string") {
        high=MIDI.noteNumber(high);
    }
    if(event.pitch >= low && event.pitch <= high) {
        return true;
    }
    else {
        return false;
    }
};

LPK.isValueRange = function(event, low, high) {
    if(event.value >= low && event.value <= high) {
        return true;
    }
    else {
        return false;
    }
};

LPK.isNumberRange = function(event, low, high) {
    if(event.value >= low && event.value <= high) {
        return true;
    }
    else {
        return false;
    }
};

LPK.isVelocityRange = function(event, low, high) {
    if(event.velocity >= low && event.velocity <= high) {
        return true;
    }
    else {
        return false;
    }
};


/***********************************************
 * MidiMatrix Class 
 ***********************************************/

// Constructor
/*
   [ { low:"C2", high:"C3", channel:1, port:1 },
     { low:"C1", high:"C2", channel:2 }
   ]
*/  
MidiMatrix = function(ranges) {   

    this.range = new Array(128);  // array of all possible pitches 

    if(ranges == undefined) return;
    
    // TODO need error checking to validate provided JSON

    // single range provided in {} instead of []
    if(typeof ranges == "object" && !Array.isArray(ranges)) { // TODO is this best way to detect?
        if(ranges.port == undefined || ranges.port < 1) ranges.port = 1;
        this.addRange(ranges.low, ranges.high, ranges.channel, ranges.port);
        return;
    }    
        
    // Multiple ranges provided in array
    if(Array.isArray(ranges)) {
        for( let i=0; i< ranges.length; i++) {
            if(ranges[i].port == undefined || ranges[i].port < 1) ranges[i].port = 1;
            this.addRange(ranges[i].low, ranges[i].high, 
                          ranges[i].channel, ranges[i].port);
        }    
        return;
    }
};


// addRange
MidiMatrix.prototype.addRange = function(low, high, channel, port) {

    if(port == undefined || port < 1) port = 1;  // default
    
    // TODO check for range duplicates or conflicts?
    
    if(typeof low == "string") low = MIDI.noteNumber(low);
    if(typeof high == "string") high = MIDI.noteNumber(high);
    
    for( let p=low; p<= high; p++) {
        if(this.range[p] == undefined) {
            this.range[p] = [];
        }
        
        let layers=this.range[p];
        let found = false;
        for( i=0; i<layers.length; i++) {
            if(layers[i].channel == channel && layers[i].port == port) {
                found=true
            }
        }
        if(!found) {
            layers.push({channel:channel, port:port});
        }
    }
};


// TODO, how to handle other types of events through zones?  
//       This might need Channelizer support.  But then Channelizer
//       could be built upon this functionlity perhaps also.  Think it through.

// send
MidiMatrix.prototype.send = function(event) {

    // if no range for this pitch, don't send
    if(this.range[event.pitch] == undefined) return;
    
    if(event.port == undefined || event.port < 1) event.port = 1;    
    let origChannel = event.channel;
    let origPort = event.port;        
    let layers=this.range[event.pitch];
    
    for( i=0; i<layers.length; i++) {   
        event.channel = layers[i].channel;
        event.port = layers[i].port;
        event.send();
    }
    
    event.channel = origChannel;
    event.port = origPort;
};



/******************************************************************
 * MidiTimer Class
 *
 * TODO - handle cycle modes correctly to reset counters when cycle
 *        happens.  This may be really complicated.
 * TODO - repeating triggers?
 * TODO - triggerMillisecondsAfterBeat - have to figure out how to 
 *        calculate that.
 * TODO - consider moving inside MIDI namespace
 ******************************************************************/

var MidiTimer = function() {
    this.triggers = [];
    this.mstriggers = [];
};


/****************************************
 * process method
 *
 * should be called once per process block
 * to update various timer counters and 
 * potentially call timer triggers
 */
MidiTimer.prototype.process = function(ctx) {

    // Date.now triggers, always, even when not playing
 
    for( let i=0; i<this.mstriggers.length; i++) {
    
        let t = this.mstriggers[i];
        let now = Date.now();
        
        if( now >= t.start + t.ms ) {
                
            let funcPtr = t.func;
            funcPtr(t.ms);
            this.mstriggers.splice(i,1); // remove the trigger
        }
    }  
    
    /*********************************
     * Only while playing?
     *********************************/
     
    if(ctx == undefined) {
        ctx = GetTimingInfo();
    }

    if (!ctx.playing) return;
    
    // BeatPos triggers
    for( let i=0; i<this.triggers.length; i++) {
        let t = this.triggers[i];
        if( t.beatPos >= ctx.blockStartBeat
                && t.beatPos <= ctx.blockEndBeat ) {
                
            let funcPtr = t.func;
            funcPtr(t.beatPos);
            this.triggers.splice(i,1); // remove the trigger
        }
    }    
};

// Set a function to be called an exact beatPos
MidiTimer.prototype.triggerAtBeat = function(func, beat) {
    this.triggers.push({
        beatPos: beat,
        func:    func
    });
};

// Set a function to be called after so many beats goes by
MidiTimer.prototype.triggerAfterBeats = function(func, beats) {

    let ctx = GetTimingInfo();

    this.triggers.push({
        beatPos: ctx.blockEndBeat + beats,
        func:    func
    });
};


// Use Date.now() as milliseconds to trigger function call, 
// Not based on BeatPos at all, just raw real time in the code
// 
MidiTimer.prototype.triggerAfterMilliseconds = function(func, ms) {
    this.mstriggers.push({
        start:  Date.now(),
        ms:     ms,
        func:   func
    });
};

//=========================================
// LPK singletons
//=========================================

LPK._parameterCache = {
   data: [],
   set: function(id, val) {
            // TODO add error message if no PluginParameters
       if(typeof id != "string") id = PluginParameters[id].name;
       this.data[id] = val;
   },
   get: function(id) {
            // TODO add error message if no PluginParameters
       if(typeof id != "string") id = PluginParameters[id].name;
       if(this.data[id] == undefined) {
           this.data[id] = GetParameter(id);
       }
       return this.data[id];
   }
};
//=================================================
// _console singleton
// kept inside LPK, typically not accessed directly
//=================================================

LPK._console = {
   maxFlush: 20,
   maxLen:   1000,
   b:[],
   log: function(msg) {this.b.push(msg)},
   setMaxFlush: function(max) {this.maxFlush = max},
   setMaxLen: function(max) {this.maxLen = max},
   flush: function() {
       var i=0;
       while(i<=this.maxFlush && this.b.length>0) {
           var line=this.b.shift();
           if(typeof line != "string") {
               line = line.toString();
           }
           var tl = line.length;
           var p = 0;
           var sub=line.substr(p,this.maxLen);
           Trace(sub);
           p += this.maxLen;
           while(p < tl) {
               sub = line.substr(p,this.maxLen);
               Trace("..." + sub);
               p += this.maxLen;
           }
           i++;
       }
   }
};


//=======================================
// Internal _sequencer singleton
//=======================================

// Use an array for top level one per beat
// each elem will be a binary tree based on 
// beatPos.  When there are more then one
// events on the same beatPos, then use an 
// array with push so that FIFO at at each 
// beatPos.  So each node of tree is 
// [{event:{}}]

LPK.BeatNode = function(nodeval, left, right) {
    this.val = [nodeval];   // Event for now, but later an outer struct
    this.left = left;
    this.right = right;
};

LPK.BeatList = function() {
    this.list = []; // top level array per beat
};

// recursive function to find right place to add node or 
// push into array of existing node with same timestamp
// need to change this maybe, so that will be by reference?
// JSON.stringify seems to pull them all in as if tree built by value

LPK.BeatList.prototype.addNode = function(node, iNodeVal) {
    if(iNodeVal.evt.beatPos == node.val[0].evt.beatPos) {
        node.val.push(iNodeVal);
        return;
    }
    if(iNodeVal.evt.beatPos < node.val[0].evt.beatPos) {
        if(node.left == undefined) {
            node.left = new LPK.BeatNode(iNodeVal, undefined, undefined);
            return;
        }
        this.addNode(node.left, iNodeVal);
        return;
    }
    if(node.right == undefined) {
        node.right = new LPK.BeatNode(iNodeVal, undefined, undefined);
        return;
    }
    this.addNode(node.right, iNodeVal);
    return;
};
    
LPK.BeatList.prototype.add = function(nodeVal) {
    let b = Math.trunc(nodeVal.evt.beatPos);
    let bt = this.list[b];
    if( bt == undefined) {
        this.list[b] = new LPK.BeatNode(nodeVal, undefined, undefined);
        return;
    }
    this.addNode(bt, nodeVal);
    return;
};


// internal singleton
LPK._sequencer = {
    beatList: (new LPK.BeatList),
    add: function(nodeVal) {
        this.beatList.add(nodeVal);
    }
};


//========================================
// Non callback wrapper functions
//========================================

LPK.Trace = function(msg) {
    LPK._console.log(msg);
};
LPK.GetParameter = function(id) {
    return LPK._parameterCache.get(id);
};
// simple wrapper for consistency.  will be cached by ParameterChanged
LPK.SetParameter = function(id, val) {
    SetParameter(id, val);
};
LPK.ParameterChanged(id, val) {
    LPK._parameterCache.set(id, val);  // cache this parameter
};
// simple wrapper
LPK.UpdatePluginParameters = function() {
    UpdatePluginParameters();
};



//====================================================
// Callback enhancer functinos.  These are meant 
// to be called from inside the normal Scripter
// Callback functions to carry out various LPK duties
// such as parameter caching, Trace flushing, status
// updating, sequence playback, etc.  users should
// always include them when using LPK toolkit.
// template provided.
//====================================================

// required
LPK.ProcessMIDI = function() {
    // TBD, this is where Sequencer will play status wil update, etc.
};

// required for parameter use
LPK.ParameterChanged = function(id, val) {
    LPK._parameterCache.set(id. val);
};

// required for buffered logging
LPK.Idle = function() {
    LPK._console.flush();
};

// optional
LPK.HandleMIDI = function(event) {
    // TBD
};

// optional
LPK.Initialize = function() {
    // TBD
};

// optional
LPK.Reset = function() {
    // TBD
};

//=========================================
// Event send wrappers
//=========================================

LPK.SendEvent = function(event) {
    // TBD, handle status tracking and durations
    event.send();
};

LPK.SendEventAtBeat = function(event, beat) {
    // TBD, handle status tracking and durations
    event.sendAtBeat(beat);
};

LPK.SendEventAfterBeat = function(event, beat) {
    // TBD, handle status tracking and durations
    event.sendAfterBeat(beat);
};

LPK.SendEventAfterMilliseconds = function(event, ms) {
    // TBD, handle status tracking and durations
    event.sendAfterMilliseconds(ms);
};


/*******************************************
 * Event extensions, should I even included
 * these?
 *******************************************/


Event.prototype.ticksFromBeat = function() {
    return LPK.ticksFromBeat(this.beatPos);
};

/*******************************************
 * number to name methods
 */
ControlChange.prototype.name = function() {
    return MIDI.ccName(this.number);
};

Note.prototype.name = function() {
    return MIDI.noteName(this.pitch);
};

PolyPressure.prototype.name = function() {
    return MIDI.noteName(this.pitch);
};

/*******************************************
 * Event type checking, sometimes with options
 * to specify specific attributes at the same
 * time
 *******************************************/

Event.prototype.isNoteOn = function() {
    return false;
};
Event.prototype.isNoteOn = function(pitch) {
    if(pitch == undefined) return true;
    if(typeof pitch == "string") pitch = MIDI.noteNumber(pitch);
    if(pitch == this.pitch) return true;
    else return false;
};

Event.prototype.isNoteOff = function() {
    return false;
};
NoteOff.prototype.isNoteOff = function(pitch) {
    if(pitch == undefined) return true;
    if(typeof pitch == "string") pitch = MIDI.noteNumber(pitch);
    if(pitch == this.pitch) return true;
    else return false;
};
NoteOn.prototype.isNoteOff = function(pitch) {
    if(pitch == undefined) {
        if(this.velocity < 1) return true;
        else return false;
    }
    else {
        if(typeof pitch == "string") pitch = MIDI.noteNumber(pitch);
        if(pitch != this.pitch) return false;

        if(this.velocity < 1) return true;
        else return false;
    }
};

Event.prototype.isControlChange = function() {
    return false;
};
ControlChange.prototype.isControlChange = function(number) {
    if(number == undefined) return true;
    if(this.number == number) return true;
    else return false;
};

Event.prototype.isPitchBend = function() {
    return false;
};
PitchBend.prototype.isPitchBend = function() {
    return true;
};

Event.prototype.isProgramChange = function() {
    return false;
};
ProgramChange.prototype.isProgramChange = function(number) {
    if(number == undefined) return true;
    if(number == this.number) return true;
    return false;
};

Event.prototype.isChannelPressure = function() {
    return false;
};
ChannelPressure.prototype.isChannelPressure = function() {
    return true;
};

Event.prototype.isPolyPressure = function() {
    return false;
};
PolyPressure.prototype.isPolyPressure = function(pitch) {
    if(pitch == undefined) return true;
    if(typeof pitch == "string") pitch = MIDI.noteNumber(pitch);
    if(pitch == this.pitch) return true;
    else return false;
};

/******************************************
 * Range checking
 ******************************************/

// Pitch Range Checking
Note.prototype.isPitchRange = function(low, high) {
    return LPK.isPitchRange(this, low, high);
};
PolyPressure.prototype.isPitchRange = function(low, high) {
    return LPK.isPitchRange(this, low, high);
};

// Velocity Range Checking
Note.prototype.isVelocityRange = function(low, high) {
    return LPK.isVelocityRange(this, low, high);
};

// ValueRange checking
ControlChange.prototype.isValueRange = function(low, high) {
    return LPK.isValueRange(this, low, high);
};
ChannelPressure.prototype.isValueRange = function(low, high) {
    return LPK.isValueRange(this, low, high);
};
PitchBend.prototype.isValueRange = function(low, high) {
    return LPK.isValueRange(this, low, high);
};

// NumberRange checking
ProgramChange.prototype.isNumberRange = function(low, high) {
    return LPK.isNumberRange(this, low, high);
};


/*
// functions user must call from inside ProcessMIDI
LPK.ProcessMIDI = function() {
    if(NeedsTimingInfo != true) {
        LPK._console.log("ERROR: NeedsTimingInfo required");
        return;
    }
    let timing = GetTimingInfo();
    LPK._sequencer.process(timing);
};
LPK.ProcessMIDI = function(timing) {
    LPK._sequencer.process(timing);
};
*/


/* consider adding set or schedule methods to Event object
 * but that would presume only one single global internal
 * queue, as opposed to being able to have more than one
 * which is managed individually
 */


