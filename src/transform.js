// Transformer
// This is interesting but its getting kind of big and complicated and 
// might not be suited for a general toolkit.  Develop it over time as 
// a seperate thing that can be optionally added to MIDIClass.js.  Perhaps later
// if it becomes more useful I will add it as a permanent member

var MidiTransform = function(json) {
};

MidiTransform.prototype.send = function(event) {
    if(this.isCondition(event)) {
        this.transform(event);
    }
    else {
        // TODO check global mode
        event.send();
    ];
};


// TODO is there an easier and cleaner way then all this verbose if/then spagetti?
MidiTransform.prototype.isCondition = function(event) {
    if(        this.isStatus(event) 
            && this.isChannel(event) 
            && this.isPort(event) 
            && this.isData1(event) 
            && this.isData2(event) 
            && this.isArticulationID(event) {
        return true;
    }
    else return false;
};


MidiTransform.prototype.isStatus = function(event) {
    if(this.action.status.toUpperCase() == "ALL"
            || (this.action.statusType.toUpperCase() == "NOTE" 
                && event instanceof Note)
            || (this.action.statusType.toUpperCase() == "NOTEON" 
                && event instanceof NoteOn)
            || (this.action.statusType.toUpperCase() == "NOTEOFF" 
                && event instanceof NoteOff)
            || (this.action.statusType.toUpperCase() == "POLYPRESSURE" 
                && event instanceof PolyPressure)
            || (this.action.statusType.toUpperCase() == "CHANNELPRESSURE" 
                && event instanceof ChannelPressure)
            || (this.action.statusType.toUpperCase() == "PROGRAMCHANGE" 
                && event instanceof ProgramChange)
            || (this.action.statusType.toUpperCase() == "CONTROLCHANGE" 
                && event instanceof ControlChange)
            || (this.action.statusType.toUpperCase() == "PITCHBEND" 
                && event instanceof PitchBend)) {
        return true;
    }
    else return false;
};


MidiTransform.prototype.isChannel = function(event) {
    if(this.condition.channel.toUpperCase() == "ALL"
            || this.condition.channel == event.channel) {
        return true;
    }
    else return false;
};

MidiTransform.prototype.isData1 = function(event) {
    if(this.condition.data1.toUpperCase() == "ALL"
            || this.condition.data1 == event.data1) {
        return true;
    }
    else return false;
};

MidiTransform.prototype.isData2 = function(event) {
    if(this.condition.data1.toUpperCase() == "ALL"
            || this.condition.data2 == event.data2) {
        return true;
    }
    else return false;
};

MidiTransform.prototype.isPort = function(event) {
    if(this.condition.port.toUpperCase() == "ALL"
            || this.condition.port == event.port) {
        return true;
    }
    else return false;
};

// TODO, might be more needed here
MidiTransform.prototype.isArticulationID = function(event) {
    if(this.condition.articulationID.toUpperCase() == "ALL"
            || this.condition.articulationID == event.articulationID) {
        return true;
    }
    else return false;
};

MidiTransform.prototype.transform = function(event) {
    this.transformStatus(event);
    this.transformChannel(event);
    this.transformData1(event);
    this.transformData2(event);
    this.transformPort(event);
    this.transformArticulationID(event);
};

// TODO can we do something smarter without using new?

MidiTransform.prototype.transformStatus = function(event) {
    let out = event;
    if(this.action.status.toUpperCase() == "FIX") {
        if(this.action.statusType.toUpperCase() == "NOTE") {
            out = new Note;
            out.channel = event.channel;
            out.
        }
    }
};


